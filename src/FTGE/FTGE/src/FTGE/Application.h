﻿#pragma once
#include "Core.h"

namespace FTGE
{
	class FTGE_API Application
	{
	public:
		Application();
		virtual ~Application();

		void Run();
	};

	Application* CreateApplication();
}
