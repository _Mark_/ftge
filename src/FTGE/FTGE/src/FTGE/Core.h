﻿#pragma once

#ifdef FTGE_PATFORM_WINDOWS
	#ifdef FTGE_BUILD_DLL
		#define FTGE_API __declspec(dllexport)
	#else
		#define FTGE_API __declspec(dllimport)
	#endif
#else
	#error FTGE supports only windows.
#endif
