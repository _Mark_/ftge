﻿#pragma once

#include <iostream>

#include "Application.h"

#ifdef FTGE_PATFORM_WINDOWS

// FTGE::Application* FTGE::CreateApplication();

int main(int args, char** argv)
{
	std::cout << "hello" << std::endl;
	auto app = FTGE::CreateApplication();
	app->Run();
	delete app;
	return 0;
}

#endif
